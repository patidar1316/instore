package com.pp.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.pp.controllers.UserController;
import com.pp.enums.Roles;
import com.pp.requests.UserCreateRequest;
import com.pp.requests.UserUpdateRequest;
import com.pp.response.Response;
import com.pp.response.UserResponse;
import com.pp.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserControllerJunit {

    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;
    
   
    @Test
    @DisplayName("Add_user")
    void TestAddNewUser_successResponse() {
        UserCreateRequest mockUserCreateRequest = new UserCreateRequest(Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", "1234", 1);
        Response<UserResponse> mockUccessfulResponse = Response.successfulResponse(200, "User Created Successfully", new UserResponse(1, Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", false, 1));
        Mockito.when(userService.addNewUser(mockUserCreateRequest)).thenReturn(mockUccessfulResponse);
        ResponseEntity<Response<UserResponse>> userResponse = userController.addNewUser(mockUserCreateRequest);
        assertNotNull(userResponse.getBody());
        assertEquals("User Created Successfully", userResponse.getBody().getMessage());
        assertEquals("rsddddd", userResponse.getBody().getData().getUserName());
    }

    @Test
    @DisplayName("find_UserBy_Id")
    void testFindUserById_ReturnSuccessResponse() {
        int userId = 1;
        Response<UserResponse> successfulResponse = Response.successfulResponse(200, "user Found with this User Id", new UserResponse(1, Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", false, 1));
        Mockito.when(userService.findUserById(userId)).thenReturn(successfulResponse);
        ResponseEntity<Response<UserResponse>> response = userController.findUserById(userId);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK.value(), response.getBody().getStatusCode());
        assertEquals("user Found with this User Id", response.getBody().getMessage());
    }
    
//    @Test
//    @DisplayName("find-all-user")
//    void testFindAllUser_RetrunSuccessResponse() {
//    	List<UserResponse> responseList=List.of(new UserResponse(1, Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", false, 1));
//    	 Response<List<UserResponse>> successfulResponse = Response.successfulResponse(200, "Retrieved Successfully ",responseList);
//    	 Mockito.when(userService.findAllUser()).thenReturn(successfulResponse);
//    	 //ResponseEntity<Response<List<UserResponse>>> response=userController.findAllUser();
//    	 assertNotNull(response.getBody());
//         assertEquals(HttpStatus.OK.value(), response.getBody().getStatusCode());
//         assertEquals("Retrieved Successfully ", response.getBody().getMessage());
//    }
    
    @Test
    @DisplayName("update_user")
    void testUpdateUser_ReturnSuccessResponse() {
    	UserUpdateRequest userUpdateRequest=new UserUpdateRequest("rrrrr","rsddddd","r@gmail.com","345345","India","123dsfv");
    	Response<UserResponse> successfulResponse=Response.successfulResponse(200, "User Updated Successfully", new UserResponse(1, Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", false, 1));
    	Mockito.when(userService.updateUser(1, userUpdateRequest)).thenReturn(successfulResponse);
    	ResponseEntity<Response<UserResponse>> userResponse=userController.updateUser(1, userUpdateRequest);
    	 assertNotNull(userResponse.getBody());
         assertEquals("User Updated Successfully", userResponse.getBody().getMessage());
    }
    
    @Test
    @DisplayName("deleteUser")
    void testDeleteUser_ReturnSuccessResponse() {
    	long userId=1;
    	 Response<UserResponse> mockDeletedUser=Response.successfulResponse("User Deleted Successfully",null);
    	 Mockito.when(userService.deleteUser(userId)).thenReturn(mockDeletedUser);
    	 ResponseEntity<Response<UserResponse>> userResponse=userController.deleteUser(1);
    	 assertEquals("User Deleted Successfully",userResponse.getBody().getMessage());
    }
    
  /**** Intrigation testing *******/
//    @Test
//    @DisplayName("All_user")
//    void testFindAllUser_successResponse() throws Exception{
//    	List<UserResponse> responseList=List.of(new UserResponse(1, Roles.ADMIN, "rrrrr", "rsddddd", "r@gmail.com", "345345", "India", false, 1));
//   	    Response<List<UserResponse>> successfulResponse = Response.successfulResponse(200, "Retrieved Successfully ",responseList);
//    	Mockito.when(userService.findAllUser()).thenReturn(successfulResponse);
//		RequestBuilder request = MockMvcRequestBuilders
//				.get("/find-all-user")
//				.accept(MediaType.APPLICATION_JSON);
//		this.mockMvc.perform(request)
//				.andExpect(status().isOk())
//				.andExpect(content().json("{\"statusCode\":200,\"message\":\"Retrieved Successfully\",\"data\":[{\"id\":1,\"role\":\"ADMIN\",\"firstName\":\"rrrrr\",\"lastName\":\"rsddddd\",\"email\":\"r@gmail.com\",\"phone\":\"345345\",\"address\":\"India\",\"active\":false,\"version\":1}]}"))
//				.andReturn();
//    }
    
    
    
}
