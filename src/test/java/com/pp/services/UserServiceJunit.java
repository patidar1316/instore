package com.pp.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;

import com.pp.enums.Roles;
import com.pp.exceptions.InstoreApplicationExceptions;
import com.pp.models.User;
import com.pp.repository.UserRepository;
import com.pp.requests.UserCreateRequest;
import com.pp.response.Response;
import com.pp.response.UserResponse;

import io.swagger.v3.oas.annotations.tags.Tag;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceJunit {

	@InjectMocks
	private UserService userService;
	@Mock
	private UserRepository userRespository;
	@Mock
	private ModelMapper modelMapper;
    
	@Nested
	@Tag(name="Add_User")
	@DisplayName("Add_User")
	class addUser {
		   UserCreateRequest mockUserCreateRequest = UserCreateRequest.builder()
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .password("password") // Assuming a default password
	                .merchantID(123)
	                .build();

	        User givenUser = User.builder()
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .activeUser(true) // Assuming the user is active
	                .password("password") // Assuming a default password
	                .resetPassword(null) // Assuming no reset password initially
	                .merchantID(123)
	                .build();

	        User savedUser = User.builder()
	                .userID(1L)
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .activeUser(true)
	                .password("password")
	                .resetPassword(null)
	                .merchantID(123)
	                .build();
	        
	        UserResponse userResponse = UserResponse.builder()
	                .userID(1L)
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .activeUser(true)
	                .merchantID(123)
	                .build();

		@Test
		@DisplayName("Success_add_user")
		void testAddUser_Returnsuccess() {

			// Mock the ModelMapper behavior
			when(modelMapper.map(mockUserCreateRequest, User.class)).thenReturn(givenUser);
			when(modelMapper.map(savedUser, UserResponse.class)).thenReturn(userResponse);

			// Mock the repository calls
			when(userRespository.existsByEmail(mockUserCreateRequest.getEmail())).thenReturn(false);
			when(userRespository.existsByUserName(mockUserCreateRequest.getUserName())).thenReturn(false);
			when(userRespository.save(givenUser)).thenReturn(savedUser);

			// Service method call
			Response<UserResponse> response = userService.addNewUser(mockUserCreateRequest);

			// Verify the response
			assertEquals(200, response.getStatusCode());
			assertEquals("User Created Successfully", response.getMessage());
			assertEquals(mockUserCreateRequest.getEmail(), response.getData().getEmail());

		}
		@Test
		@DisplayName("Add_user_throws_exception_when_email_exists")
		void givenEmail_whenEmailExistingAlready_ThenExceptionThrown() {
			when(modelMapper.map(mockUserCreateRequest, User.class)).thenReturn(givenUser);
			
			// Mock the repository calls
			when(userRespository.existsByEmail(mockUserCreateRequest.getEmail())).thenReturn(true);
			 InstoreApplicationExceptions exception = assertThrows(InstoreApplicationExceptions.class, () -> {
		            userService.addNewUser(mockUserCreateRequest);
		        });
			 assertEquals("User already exist with same Email", exception.getMessage());
		
     	}
		
		@Test
		@DisplayName("Add_user_throws_exception_when_username_exists")
		void givenEmail_whenUserNameExistingAlready_ThenExceptionThrown() {
			when(modelMapper.map(mockUserCreateRequest, User.class)).thenReturn(givenUser);
			
			// Mock the repository calls
			when(userRespository.existsByUserName(mockUserCreateRequest.getUserName())).thenReturn(true);
			 InstoreApplicationExceptions exception = assertThrows(InstoreApplicationExceptions.class, () -> {
		            userService.addNewUser(mockUserCreateRequest);
		        });
			 assertEquals("User already exist with same user name", exception.getMessage());
		
     	}
	}

	@Nested
	@Tag(name="findUserBy_Id")
	@DisplayName("findUserBy_Id")
	class findUserById {
		   User mockUser = User.builder()
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .password("password") 
	                .merchantID(123)
	                .build();
		   
		   UserResponse userResponse = UserResponse.builder()
	                .userID(1L)
	                .role(Roles.ADMIN)
	                .fullName("John Doe")
	                .userName("johndoe")
	                .email("johndoe@example.com")
	                .mobileNumber("1234567890")
	                .country("USA")
	                .activeUser(true)
	                .merchantID(123)
	                .build();
		   
		void findUserById_successResponse() {
			long id=1;
			when(modelMapper.map(mockUser, UserResponse.class)).thenReturn(userResponse);
			when(modelMapper.map(mockUser, UserResponse.class)).thenReturn(userResponse);
//			when(userRespository.findById(id)).thenReturn(true);
//			when(userRespository.findById(id)).thenThrow(null)
		}
	}
	
	
}
