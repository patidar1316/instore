package com.pp.services;

import com.pp.exceptions.InstoreApplicationExceptions;
import com.pp.models.Store;
import com.pp.requests.StoreCreatedRequest;
import com.pp.requests.StoreUpdateRequest;
import com.pp.response.Response;
import com.pp.response.StoreResponse;
import com.pp.response.UserResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StoreService {
//    @Autowired
//    private StoreRepository storeRepository;
//
//    ModelMapper modelMapper = new ModelMapper();
//
//    public Response<StoreResponse> addStore(StoreCreatedRequest storeCreatedRequest) {
//        var storeCreatedRequestobj = modelMapper.map(storeCreatedRequest, Store.class);
//
//        if (storeRepository.existsByStoreName(storeCreatedRequestobj.getStoreName()))
//            throw new InstoreApplicationExceptions("Store is  already exist with same name");
//        var savedObj = storeRepository.save(storeCreatedRequestobj);
//        var storeResponse = modelMapper.map(savedObj, StoreResponse.class);
//        return Response.successfulResponse("Store Created Successfully", storeResponse);
//    }
//
//    public Response<StoreResponse> findStoreById(Integer id) {
//        var fetchedData = storeRepository.findById(id).orElseThrow(() -> new InstoreApplicationExceptions("Store is not found by {}" + id));
//        var storeResponse = modelMapper.map(fetchedData, StoreResponse.class);
//        return Response.successfulResponse("Store fetched successfully", storeResponse);
//    }
//
//    public Response<List<StoreResponse>> findAllStore() {
//        var fetchedData = storeRepository.findAll();
//        List<StoreResponse> storeResponse = fetchedData.stream().map(storeObj -> modelMapper.map(storeObj, StoreResponse.class)).collect(Collectors.toList());
//        return Response.successfulResponse("All Store fetched successfully", storeResponse);
//    }
//
//    public Response<StoreResponse> deleteStoreById(Integer id) {
//        Optional<Store> fetchedStore = storeRepository.findById(id);
//        if (fetchedStore.isPresent()) {
//            storeRepository.deleteById(id);
//            return Response.successfulResponse("Store deleted Successfully", null);
//        }
//        return Response.successfulResponse("Store does not exist with provided Id", null);
//    }
//
//    public Response<StoreUpdateRequest> updateStore(Integer id, StoreUpdateRequest storeUpdateRequest) {
//        Optional<Store> allStore = storeRepository.findById(id);
//        if (allStore.isPresent()) {
//            Store storeObj = allStore.get();
//            storeObj.setStoreName(storeUpdateRequest.getStoreName());
//            storeObj.setCity(storeUpdateRequest.getCity());
//            storeObj.setLocation(storeUpdateRequest.getLocation());
//            storeObj.setCity(storeObj.getCity());
//            storeObj.setCountryISO(storeUpdateRequest.getCountryISO());
//            storeObj.setMerchantID(storeUpdateRequest.getMerchantID());
//        }
//        return null;
//    }
}
