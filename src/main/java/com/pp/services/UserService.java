package com.pp.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.pp.exceptions.InstoreApplicationExceptions;
import com.pp.jpaspecification.UserSpecification;
import com.pp.models.User;
import com.pp.repository.UserRepository;
import com.pp.requests.UserCreateRequest;
import com.pp.requests.UserUpdateRequest;
import com.pp.response.PageResponse;
import com.pp.response.Response;
import com.pp.response.UserResponse;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	ModelMapper modelMapper = new ModelMapper();

	public Response<UserResponse> addNewUser(UserCreateRequest userCreateRequest) throws InstoreApplicationExceptions {
		var createUserObj = modelMapper.map(userCreateRequest, User.class);
		if (userRepository.existsByEmail(createUserObj.getEmail()))
			throw new InstoreApplicationExceptions("User already exist with same Email");
		if (userRepository.existsByUserName(createUserObj.getUserName()))
			throw new InstoreApplicationExceptions("User already exist with same user name");
		var savedObj = userRepository.save(createUserObj);
		var userResponse = modelMapper.map(savedObj, UserResponse.class);
		return Response.successfulResponse("User Created Successfully", userResponse);
	}

	public Response<UserResponse> findUserById(long id) throws InstoreApplicationExceptions {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new InstoreApplicationExceptions("User not available with this Id : " + id));
		var userResponse = modelMapper.map(user, UserResponse.class);
		return Response.successfulResponse("user Found with this User Id", userResponse);
	}

	public PageResponse<UserResponse> findAllUser(String fullname, String mobileNumber, String country, String activeUser, String role, int page, int size, String[] sortBy, String searchBy) {
		Page<UserResponse> pageResponse = null;
		Sort.Direction sortDirection = Sort.Direction.fromString(sortBy[1]);
		Sort sortOrder = Sort.by(sortDirection, sortBy[0]);
		Pageable pageable = PageRequest.of(page, size, sortOrder);
		
		if(Objects.nonNull(searchBy)) {
			pageResponse = userRepository.findUserBySearch(searchBy, pageable);
		}else {
			Map<String, Object> filter = new HashMap<>();
			filter.put(UserSpecification.FULLNAME, fullname);
			filter.put(UserSpecification.MOBILE_NUMBER, mobileNumber);
			filter.put(UserSpecification.ROLE, role);
			filter.put(UserSpecification.COUNTRY, country);
			filter.put(UserSpecification.ACTIVE_USER, activeUser);
			Specification<User> specification = new UserSpecification(filter);
			
			Page<User> allUser = userRepository.findAll(specification,pageable);
			pageResponse = allUser.map(userObj -> modelMapper.map(userObj, UserResponse.class));
		}
		
		 return PageResponse.createGenericPagedResponse(pageResponse);
		
	}

	public Response<UserResponse> updateUser(long id, UserUpdateRequest userUpdateRequest) {
		Optional<User> userOptional = userRepository.findById(id);
		if (userOptional.isPresent()) {
			User userObj = userOptional.get();
			userObj.setUserName(userUpdateRequest.getUserName());
			userObj.setFullName(userUpdateRequest.getFullName());
			userObj.setEmail(userUpdateRequest.getEmail());
			userObj.setMobileNumber(userUpdateRequest.getMobileNumber());
			userObj.setCountry(userUpdateRequest.getCountry());
			userObj.setPassword(userUpdateRequest.getPassword());
			var updatedObj = modelMapper.map(userRepository.save(userObj), UserResponse.class);
			return Response.successfulResponse(200, "User Updated Successfully", updatedObj);
		} else
			return Response.successfulResponse(500, "Something wrong to update the user", null);
	}

	public Response<UserResponse> deleteUser(Long id) {
		Optional<User> userObj = userRepository.findById(id);
		if (userObj.isPresent()) {
			userRepository.deleteById(id);
			return Response.successfulResponse("User Deleted Successfully", null);
		} else {
			return Response.successfulResponse("User does not exist", null);

		}

	}

	public Response<List<UserResponse>> findUserByName() {
		Specification<User> specification = new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.equal(root.get("userName"), "kjasdfhncn");
			}

		};
		List<User> user = userRepository.findAll(specification);
		List<UserResponse> userResponse = user.stream().map(userObj -> modelMapper.map(userObj, UserResponse.class))
				.collect(Collectors.toList());
		return Response.successfulResponse("user Found with this User Id", userResponse);
	}
}
