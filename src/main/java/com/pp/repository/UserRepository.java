package com.pp.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pp.models.User;
import com.pp.response.UserResponse;


public interface UserRepository extends JpaRepository<User, Long>,JpaSpecificationExecutor<User> {


    Optional<User> findByMobileNumber(String mobileNumber);

    Optional<User> findByEmail(String email);

    Boolean existsByUserName(String userName);

    Boolean existsByMobileNumber(String mobileNumber);

    Boolean existsByEmail(String email);


    Optional<User> findByUserNameOrEmail(String username, String email);

    @Query("SELECT new com.pp.response.UserResponse(u.userID, u.role, u.fullName, u.userName, u.email, u.mobileNumber, u.country, u.activeUser, u.merchantID) " +
    	       "FROM user u " +
    	       "WHERE CONCAT(u.fullName, u.userName, u.mobileNumber, u.country, u.email, " +
    	       "CASE WHEN u.activeUser = TRUE THEN 'true' ELSE 'false' END)" +
    	       "LIKE %:searchBy%")
    	Page<UserResponse> findUserBySearch(@Param("searchBy") String searchBy, Pageable pageable);
  

}
