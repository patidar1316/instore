package com.pp.repository;

import com.pp.models.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store ,Integer> {
    boolean existsByStoreName(String storeName);
}
