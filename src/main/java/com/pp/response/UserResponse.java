package com.pp.response;

import com.pp.enums.Roles;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponse {

    private long userID;
    private Roles role;
    private String fullName;
    private String userName;
    private String email;
    private String mobileNumber;
    private String country;
    private Boolean activeUser;
    private int merchantID;
}
