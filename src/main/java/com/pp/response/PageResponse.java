package com.pp.response;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@Data
public class PageResponse<T> {

	private boolean success;
	private int statusCode;
	private String message;
	private int page;
	private int size;
	private long totalItems;
	private int totalPages;
	private boolean lastPage;
	private Object data;

	public static <T> PageResponse<T> createGenericPagedResponse(Page<T> paginatedData) {
		var response = new PageResponse<T>();
		response.setSuccess(true);
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setPage(paginatedData.getNumber());
		response.setSize(paginatedData.getSize());
		response.setTotalItems(paginatedData.getTotalElements());
		response.setTotalPages(paginatedData.getTotalPages());
		response.setData(paginatedData.getContent());
		return response;
	}
}