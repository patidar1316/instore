package com.pp.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoreResponse {
    private int storeID;
    private String storeName;
    private String location;
    private String city;
    private String countryISO;
    private String about;
    private boolean activeStore;
    private long merchantID;
}
