package com.pp.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long countryID;
    @Column(name = "countryCode", unique = true, nullable = false)
    private String countryCode;
    @Column(name = "countryName", unique = true, nullable = false)
    private String countryName;
    @Column(name = "currency", unique = true, nullable = false)
    private String currency;
    @Column(name = "countryCodeTwoDigit", unique = true, nullable = false)
    private String countryCodeTwoDigit;

}
