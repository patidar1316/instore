package com.pp.jpaspecification;

import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.pp.enums.Roles;
import com.pp.models.User;

public class UserSpecification implements Specification<User> {

	public static final String FULLNAME = "fullName";
	public static final String ROLE = "role";
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String COUNTRY = "country";
	public static final String ACTIVE_USER = "activeUser";

	private transient Map<String, Object> filter;

	public UserSpecification(Map<String, Object> filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		 Predicate p = criteriaBuilder.conjunction();
		 if (filter.getOrDefault(UserSpecification.FULLNAME, null) != null) {
			 p.getExpressions().add(criteriaBuilder.equal(root.get("fullName"),filter.get(UserSpecification.FULLNAME)));
	        }
		 if (filter.getOrDefault(UserSpecification.MOBILE_NUMBER,null) != null) {
			 p.getExpressions().add(criteriaBuilder.equal(root.get("mobileNumber"), filter.get(UserSpecification.MOBILE_NUMBER)));
	        }
	        if (filter.getOrDefault(UserSpecification.COUNTRY,null) != null) {
	        	p.getExpressions().add(criteriaBuilder.equal(root.get("country"), filter.get(UserSpecification.COUNTRY)));
	        }
	        if (filter.getOrDefault(UserSpecification.ACTIVE_USER,null) != null) {
	        	p.getExpressions().add(criteriaBuilder.equal(root.get("activeUser"), Boolean.valueOf((String) filter.get(UserSpecification.ACTIVE_USER))));
	        }
	      //when we can do filter of Enum type value  (here SettlementStatus is Enum)
			if (filter.getOrDefault(UserSpecification.ROLE,null) != null) {
				Roles role = Roles.valueOf(filter.get(UserSpecification.ROLE).toString());
				p.getExpressions().add(criteriaBuilder.equal(root.get(UserSpecification.ROLE), role));
            }

	        return p;
	    }

}