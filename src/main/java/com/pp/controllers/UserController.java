package com.pp.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pp.requests.UserCreateRequest;
import com.pp.requests.UserUpdateRequest;
import com.pp.response.PageResponse;
import com.pp.response.Response;
import com.pp.response.UserResponse;
import com.pp.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/save-user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<UserResponse>> addNewUser(@Valid @RequestBody UserCreateRequest user) {
		var response = userService.addNewUser(user);
		return ResponseEntity.status(response.getStatusCode()).body(response);
	}

	@GetMapping(value = "/find-by-id/{id}")
	public ResponseEntity<Response<UserResponse>> findUserById(@PathVariable long id) {
		var response = userService.findUserById(id);
		return ResponseEntity.status(response.getStatusCode()).body(response);
	}

	@GetMapping(value = "/find-all-user")
	public ResponseEntity<PageResponse<UserResponse>> findAllUser(
			@RequestParam(value = "fullName", required = false) String fullName,
			@RequestParam(value = "mobileNumber", required = false) String mobileNumber,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "activeUser", required = false) String activeUser,
			@RequestParam(value = "role", required = false) String role,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value =  "size", defaultValue = "10") int size,
			@RequestParam(value = "sortBy", defaultValue = "userID,asc") String[] sortBy,
			@RequestParam(value = "searchBy", required = false) String searchBy
			){
		var response = userService.findAllUser(fullName,mobileNumber, country, activeUser, role, page, size, sortBy, searchBy);
		return ResponseEntity.status(response.getStatusCode()).body(response);
	}

	@PatchMapping(value = "/update/{id}")
	public ResponseEntity<Response<UserResponse>> updateUser(@PathVariable long id,
			@RequestBody UserUpdateRequest userUpdateRequest) {
		var userUpdateObj = userService.updateUser(id, userUpdateRequest);
		return ResponseEntity.status(userUpdateObj.getStatusCode()).body(userUpdateObj);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Response<UserResponse>> deleteUser(@PathVariable long id) {
		var deletedObj = userService.deleteUser(id);
		return ResponseEntity.status(deletedObj.getStatusCode()).body(deletedObj);
	}

	@PostMapping("/specification")
	public ResponseEntity<Response<List<UserResponse>>> findUserByName() {
		var response = userService.findUserByName();
		return ResponseEntity.status(response.getStatusCode()).body(response);
	}
}
