package com.pp.controllers;

import com.pp.requests.StoreCreatedRequest;
import com.pp.requests.StoreUpdateRequest;
import com.pp.response.Response;
import com.pp.response.StoreResponse;
import com.pp.response.UserResponse;
import com.pp.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//import javax.validation.Valid;
//import java.util.List;

@RestController
@RequestMapping("/storeApi")
public class StoreController {

//    @Autowired
//    private StoreService storeService;
//
//    @PostMapping
//    public ResponseEntity<Response<StoreResponse>> addStore(@Valid @RequestBody StoreCreatedRequest storeCreatedRequest) {
//        var SavedObj = storeService.addStore(storeCreatedRequest);
//        return ResponseEntity.status(SavedObj.getStatusCode()).body(SavedObj);
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<Response<StoreResponse>> findStoreByID(@PathVariable Integer id) {
//        var fetchedStoreData = storeService.findStoreById(id);
//        return ResponseEntity.status(fetchedStoreData.getStatusCode()).body(fetchedStoreData);
//    }
//
//    @GetMapping
//    public ResponseEntity<Response<List<StoreResponse>>> findAllStore() {
//        var fetchedAllStore = storeService.findAllStore();
//        return ResponseEntity.status(fetchedAllStore.getStatusCode()).body(fetchedAllStore);
//    }
//
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Response<StoreResponse>> storeDeleteById(@PathVariable Integer id) {
//        var deletedStore = storeService.deleteStoreById(id);
//        return ResponseEntity.status(deletedStore.getStatusCode()).body(deletedStore);
//    }
//
//    @PatchMapping(value = "/update/{id}")
//    public ResponseEntity<Response<StoreUpdateRequest>> updateStore(@PathVariable Integer id ,@RequestBody StoreUpdateRequest storeUpdateRequest) {
//        var updatedStore = storeService.updateStore(id,storeUpdateRequest);
//        return ResponseEntity.status(updatedStore.getStatusCode()).body(updatedStore);
//    }
}
