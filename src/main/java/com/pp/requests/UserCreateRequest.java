package com.pp.requests;

import javax.validation.constraints.*;

import com.pp.enums.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserCreateRequest {

    private Roles role;
    
    
    @NotBlank(message = "Please enter Full Name")
    @Pattern(regexp="^[A-Za-z]+$", message="Name can have only Alphabets")
    private String fullName;
    
    @NotBlank(message = "Please enter User Name")
    @Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{1,8}$", message="User name is not correct")
    private String userName;
    
    @Email(message = "Please enter a valid email Id")
    @NotBlank(message = "Please enter Email")
    private String email;
    
    @NotBlank(message = "Please enter Mobile")
    @Pattern(regexp="^\\d+$", message="Mobile number can have only number")
    private String mobileNumber;
    
    @NotBlank(message = "Please enter Country")
    private String country;
    
    @NotBlank(message = "Please enter Password")
    @Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$", message="password is not correct")
    private String password;
    
    private int merchantID;

}
