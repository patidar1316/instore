package com.pp.requests;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreCreatedRequest {

    private int storeID;
    @NotBlank(message = "please enter the store name")
    private String storeName;
    private String location;
    private String city;
    private String countryISO;
    private String about;
    private boolean activeStore;
    private long merchantID;
}
