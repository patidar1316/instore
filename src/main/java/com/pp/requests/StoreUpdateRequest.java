package com.pp.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class StoreUpdateRequest {
    private int storeID;
    private String storeName;
    private String location;
    private String city;
    private String countryISO;
    private String about;
    private boolean activeStore;
    private long merchantID;
}
